racket-tiled
============
Racket Library for parsing the xml output of the [Tiled](https://www.mapeditor.org/) editor. A struct with the necessary data is produced which can be used by other 2D frameworks to display the maps